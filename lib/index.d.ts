import * as React from 'react';
interface TextStyle {
    fontFamily?: string;
    fontSize?: number;
    color?: string;
}
interface Props {
    checked: boolean;
    bgChecked?: string;
    bgUnChecked?: string;
    bdUnChecked?: string;
    iconSize?: number;
    text?: string;
    textStyle?: TextStyle;
    onPress: Function;
}
declare const AnimatedCheckbox: React.FunctionComponent<Props>;
export default AnimatedCheckbox;
