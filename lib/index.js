"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_native_1 = require("react-native");
const Feather_1 = __importDefault(require("react-native-vector-icons/Feather"));
const styles = react_native_1.StyleSheet.create({
    container: {
        padding: 8,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    boxContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderRadius: 6,
    },
    text: {
        marginLeft: 8,
        fontSize: 16,
    },
});
const AnimatedCheckbox = ({ checked, bgChecked, bgUnChecked, bdUnChecked, iconSize, text, textStyle, onPress, }) => {
    const [scale] = React.useState(new react_native_1.Animated.Value(1));
    const handlePress = () => {
        scale.setValue(0.7);
        react_native_1.Animated.spring(scale, {
            toValue: 1,
            friction: 3,
        }).start();
        onPress();
    };
    return (<react_native_1.TouchableOpacity activeOpacity={0.6} style={styles.container} onPress={handlePress}>
      <react_native_1.Animated.View style={{
        ...styles.boxContainer,
        backgroundColor: checked ? bgChecked : bgUnChecked,
        borderColor: checked ? bgChecked : bdUnChecked,
        transform: [{ scale }],
    }}>
        <Feather_1.default name="check" size={iconSize} color="#FFFFFF"/>
      </react_native_1.Animated.View>
      <react_native_1.Text style={{ ...styles.text, ...textStyle }}>{text}</react_native_1.Text>
    </react_native_1.TouchableOpacity>);
};
AnimatedCheckbox.defaultProps = {
    checked: false,
    bgChecked: '#0154C6',
    bdUnChecked: '#979797',
    bgUnChecked: '#FFFFFF',
    iconSize: 20,
    text: 'Example text',
    textStyle: {},
};
exports.default = AnimatedCheckbox;
